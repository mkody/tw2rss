<?php
if (!file_exists($file = __DIR__ . '/vendor/autoload.php')) {
  throw new \Exception('please run "composer install" in "' . __DIR__ .'"');
}
require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/config.php';

use Suin\RSSWriter\Channel;
use Suin\RSSWriter\Feed;
use Suin\RSSWriter\Item;

foreach ($accounts as $a) {
  $statuses = $twitter->load(
    Twitter::ME + Twitter::RETWEETS,
    20,
    array(
      'screen_name' => $a,
      'exclude_replies' => true,
      'tweet_mode' => 'extended'
    )
  );

  $feed = new Feed();

  $channel = new Channel();
  $channel
    ->title('@' . $a)
    ->description('Tweets de @' . $a)
    ->url('https://twitter.com/' . $a)
    ->feedUrl('https://kdy.ch/tw2rss/' . $a . '.xml')
    ->pubDate(time())
    ->lastBuildDate(time())
    ->ttl(15)
    ->appendTo($feed);

  foreach ($statuses as $status) {
    // In case of a retweet, use the retweeted status
    if (isset($status->retweeted_status)) $status = $status->retweeted_status;
    // I guess we still have to build the tweet URL ourselves
    $url = 'https://twitter.com/' . $status->user->screen_name . '/status/' . $status->id_str;

    $item = new Item();
    $item
      ->title('Tweet de ' . $status->user->name)
      ->description(nl2br(Twitter::clickable($status)))
      ->contentEncoded(nl2br(Twitter::clickable($status)))
      ->url($url)
      ->author($status->user->name . ' (@' . $status->user->screen_name . ')')
      ->pubDate(strtotime($status->created_at))
      ->guid($url, true)
      ->preferCdata(true)
      ->appendTo($channel);
  }

  file_put_contents('public/' .  $a . '.xml', $feed->render());
}
